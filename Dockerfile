FROM registry.gitlab.com/mrponpon/timber-detection-ai:env_base_cpu
WORKDIR /ai
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
CMD uvicorn app.main:app --host "0.0.0.0" --port "3333" --workers 1