from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.routes import views
from app.utils.class_models import Yolov5_pytorch
import os
import glob
import urllib3
urllib3.disable_warnings()
app = FastAPI()
print(os.getcwd())

@app.on_event("startup")
def compiling_models():
    app.timber_ref_doublea = Yolov5_pytorch("app/weights/timber_ref_doubleav1.pt",device="")
    app.timber_ref_blueobj = Yolov5_pytorch("app/weights/timber_ref_blueobjv1.pt",device="")
    app.lumber_detect = Yolov5_pytorch("app/weights/lumber_detectv2.pt",device="")
    app.timber_detect = Yolov5_pytorch("app/weights/timber_detectv2.pt",device="")



# Set all CORS enabled origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(views.router)