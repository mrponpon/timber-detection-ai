from fastapi import APIRouter, Depends, Request
from app.apis.predict.predict_ai import predict_image
from app.apis.healthcheck.health_check import healthChecking
from pydantic import BaseModel
from starlette.responses import JSONResponse
from typing import List, Optional

class Item(BaseModel):
    img: str
    user= "other"
    length_wood: Optional[float]
    small_case= 0.0
    medium_case= 0.0
    large_case= 0.0


router = APIRouter()

@router.get("/")
def index():
    return "HELLO AI API"

@router.get("/api/v1/health-check")
def view_1(request: Request):
    return JSONResponse(content = healthChecking(request), status_code = 200)

@router.post("/api/v1/detection", tags=["api_predict"])
def view_2(request: Request,data: Item):
    return JSONResponse(content = predict_image(request,data), status_code = 200)