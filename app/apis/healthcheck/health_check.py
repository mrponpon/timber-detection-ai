import numpy as np
import cv2

def healthChecking(request):
    print("health checking..")
    image = np.random.randint(255, size=(100,100,3),dtype=np.uint8)
    request.app.timber_ref_doublea.detect(image)
    request.app.timber_ref_blueobj.detect(image)
    request.app.lumber_detect.detect(image)
    request.app.timber_detect.detect(image)
    return {"message":"THE API IS HEALTHY."}