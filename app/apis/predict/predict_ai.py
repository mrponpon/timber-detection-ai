import sys
import os
import numpy as np
import base64
import json
import cv2
import time
import datetime
from app.utils import detector_utils
import uuid
import threading
import bisect

doublea_ref = float(os.getenv("doublea_ref", default=3.6))
blue_ref = float(os.getenv("blue_ref", default=26.5))
timber_th = float(os.getenv("timber_th", default=0.5))
crop_timber_th = float(os.getenv("crop_timber_th", default=0.95))
ref_th = float(os.getenv("ref_th", default=0.7))

def plot_one_box(x, img, color=None, label=None, line_thickness=None):
    # Plots one bounding box on image img
    tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
    color = color or [random.randint(0, 255) for _ in range(3)]
    c1, c2 = (x["xmin"],x["ymin"]), (x["xmax"],x["ymax"])
    # print(tl / 4)
    cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)  # font thickness

        t_size = cv2.getTextSize(label, 0, fontScale=tl / 4, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        textX = (x["xmin"]+x["xmax"] - t_size[0]) // 2
        textY = (x["ymin"]+x["ymax"] + t_size[1]) // 2

        cv2.rectangle(img,  (textX-1, textY+2), (textX + t_size[0]+1, textY - t_size[1]-2), [255,255,255], -1, cv2.LINE_AA)  # filled
        # print(textX,textY)
        cv2.putText(img, label, (textX, textY), 0, tl / 4, [0, 0, 0], thickness=1, lineType=cv2.LINE_AA)

def crop_woods(img,xyxy):
    xmin, ymin, xmax, ymax = xyxy["xmin"],xyxy["ymin"],xyxy["xmax"],xyxy["ymax"]
    try:
        c_img = img[ymin:ymax+50, xmin:xmax]
    except:
        c_img = img[ymin:ymax, xmin:xmax]
    # cv2.imwrite('crop_woods.jpg', c_img)
    return c_img

def get_ref_pixel(img,user,xyxy):
    xmin, ymin, xmax, ymax = xyxy["xmin"],xyxy["ymin"],xyxy["xmax"],xyxy["ymax"]
    ref_pixel_y = abs(ymax - ymin)
    ref_pixel_x = abs(xmax - xmin)
    c_img = img[ymin:ymax, xmin:xmax]
    # cv2.imwrite('ref_object.jpg', c_img)
    # print("ref_pixel_x",ref_pixel_x)
    # print("ref_pixel_y",ref_pixel_y )
    return ref_pixel_x

def cal_wood(frame,xyxy,values_list,keys_list,wood_size_count,wood_volume,ref_inch,ref_pixel,length_wood):
    xmin, ymin, xmax, ymax = xyxy["xmin"],xyxy["ymin"],xyxy["xmax"],xyxy["ymax"]
    # Cal wood diameters assume x~y then diameter(image) of wood ~ avg(x,y)
    wood_diameter_pixel = round(((xmax - xmin) + (ymax - ymin)) / 2.0, 2)  # avg wood diameter(image) in pixel unit
    # Convert pixel to inch
    pixel2inch = ref_inch / ref_pixel
    wood_diameter_inch = (wood_diameter_pixel * pixel2inch)+0.6
    # Convert inch to meter
    wood_diameter_meter = round((wood_diameter_inch * 2.54)/100.0, 2)
    # Cal wood radius
    wood_radius_pixel = round(wood_diameter_pixel / 2.0) # wood radius(image) in pixel unit
    # Set color mask each size(S,M,L)
    result = bisect.bisect_right(values_list, wood_diameter_inch)
    if result == 0:
        result = 'discard'
    else:
        result = keys_list[result-1]

    if result == 'discard':
        wood_size = 'Discard'
        color = [0,127,255]
        wood_size_count['Discard'] += 1
    elif result == 'small_case':
        wood_size = 'Small'
        color = [0,0,255]
        wood_size_count['Small'] += 1
    elif result == 'medium_case':
        wood_size = 'Medium'
        color = [0,255,255]
        wood_size_count['Medium'] += 1
    elif result == 'large_case':
        wood_size = 'Large'
        color = [255,0,0]
        wood_size_count['Large'] += 1
    # Calculate volume
    if length_wood != None:
        volume = ((3.14 * pow(wood_diameter_meter,2))/4.0) * float(length_wood)
        wood_volume[wood_size] += float('%.2f' % round(volume,2))

    label = f'{round(wood_diameter_inch,2)}"'
    plot_one_box(xyxy, frame, label=label, color=color, line_thickness=1)

def draw_image(frame, xyxy_list,length_wood,summary_case=[],ref_pixel=100,ref_inch=11.69):
    wood_size_count = {'Discard':0,'Small':0,'Medium':0,'Large':0}
    wood_volume = wood_size_count.copy()
    keys_list = []
    values_list = []
    for i in summary_case:
        keys_list.append(list(i.keys())[0])
        values_list.append(list(i.values())[0])
        
    # print("keys_list %s" % (keys_list))
    # print("values_list %s" % (values_list))
    threads = []
    # Write results
    for xyxy in xyxy_list:
        x = threading.Thread(target=cal_wood, args=(frame,xyxy,values_list,keys_list,wood_size_count,wood_volume,ref_inch,ref_pixel,length_wood))
        threads.append(x)
        x.start()
    for index, thread in enumerate(threads):
        thread.join()

    wood_size_count['Pass'] = wood_size_count['Small'] + wood_size_count['Medium'] + wood_size_count['Large']
    wood_size_count['Total'] = wood_size_count['Pass'] + wood_size_count['Discard']
    wood_size_count['unit'] = 'inch'
    total = 0
    for k,v in wood_volume.items():
        wood_volume[k] = float('%.3f' % round(v,3))
        if wood_volume[k] > 0.0:
            total += wood_volume[k]
    wood_volume['Total'] = float('%.3f' % round(total,3))
    wood_volume['unit'] = 'cubic_meter'

    return frame, wood_size_count, wood_volume

def predict_image(request, data):
    case_dict = {"small_case":data.small_case,"medium_case":data.medium_case,"large_case":data.large_case}
    length_wood = data.length_wood
    user = data.user

    # print(length_wood,user,case_dict)
    timber_ref_doublea = request.app.timber_ref_doublea
    timber_ref_blueobj = request.app.timber_ref_blueobj
    lumber_detect = request.app.lumber_detect
    timber_detect = request.app.timber_detect

    date_time = datetime.datetime.now()
    print(f'PREDICT DATE: {date_time}')
    image = base64.b64decode(data.img)
    npimg = np.frombuffer(image, np.uint8)
    frame = cv2.imdecode(npimg, cv2.IMREAD_ANYCOLOR)
    height, width,_ = frame.shape

    #Reshape from large frame
    if  frame.shape[0] > 3600 or frame.shape[1] > 3600:
        frame = cv2.resize(frame, (int(frame.shape[1]/3.5), int(frame.shape[0]/3.5)))
        print("after_reshape %s,%s" %(frame.shape[0],frame.shape[1]))

    elif  frame.shape[0] > 2500 or frame.shape[1] > 2500:
        frame = cv2.resize(frame, (int(frame.shape[1]/2.5), int(frame.shape[0]/2.5)))
        print("after_reshape %s,%s" %(frame.shape[0],frame.shape[1]))

    elif  frame.shape[0] > 1700 or frame.shape[1] > 1700:
        frame = cv2.resize(frame, (int(frame.shape[1]/1.5), int(frame.shape[0]/1.5)))
        print("after_reshape %s,%s" %(frame.shape[0],frame.shape[1]))
    else:
        print("img shape: ",frame.shape)
    if len(frame.shape) == 2:
        frame = cv2.merge([frame,frame,frame])
        
    result_lumber = lumber_detect.detect(frame,img_size=(640, 640),conf_thres=crop_timber_th,iou_thres=0.45,max_det=1)
    # print(result_lumber)
    if not result_lumber["wood"]:
        crop_frame = frame.copy()
    else:
        xyxy_result_lumber = result_lumber["wood"][0]
        crop_frame =  crop_woods(frame,xyxy_result_lumber)
    #reshape img to base64#  
    reimencoded = cv2.imencode(".jpg", crop_frame)[1]
    reshape_img_base64 = base64.b64encode(reimencoded).decode('utf-8')
    #reshape img to base64# 

    #Check old user#
    if user == 'panjapon':
        case_defult = [{'small_case':2},{'large_case':2.5}]
        ref_inch = blue_ref
        pred_ref = timber_ref_blueobj.detect(crop_frame,img_size=(640, 640),conf_thres=ref_th,iou_thres=0.45,max_det=1)

    elif user == 'doublea':
        case_defult = [{'small_case':1.5},{'medium_case':2},{'large_case':4}]
        ref_inch = doublea_ref
        pred_ref = timber_ref_doublea.detect(crop_frame,img_size=(640, 640),conf_thres=ref_th,iou_thres=0.45,max_det=1)

    else:
        case_defult = [{'small_case':1.5},{'medium_case':2},{'large_case':4}]
        ref_inch = blue_ref
        pred_ref = timber_ref_blueobj.detect(crop_frame,img_size=(640, 640),conf_thres=ref_th,iou_thres=0.45,max_det=1)
    # print(user,pred_ref)

    if not pred_ref["ref"]:
        ref_object_detect = False
        if user == "doublea":
            ref_pixel = 60
        else:
            ref_pixel = 360
    else:
        ref_object_detect = True
        xyxy_pred_ref = pred_ref["ref"][0]
        ref_pixel = get_ref_pixel(crop_frame,user,xyxy_pred_ref)

    summary_case = []
    for case in case_defult:
        case = list(case.keys())[0]
        if case_dict[case] != 0:
            summary_case.append({case:case_dict[case]})
    if not summary_case:
        summary_case = case_defult

    # print("case size %s" %(summary_case))
    result_timber = timber_detect.detect(crop_frame,img_size=(640, 640),conf_thres=timber_th,iou_thres=0.45,max_det=1200)
    xyxy_timber_list = result_timber["wood"]
    frame, wood_size_count, wood_volume = draw_image(crop_frame, xyxy_timber_list,length_wood,summary_case,ref_pixel,ref_inch)
    img_result = cv2.imencode(".jpg", frame)[1]
    img_result_base64 = base64.b64encode(img_result).decode('utf-8')
    result_dict = {
                    'result_img':img_result_base64,
                    'wood_volume':wood_volume,
                    'wood_count':wood_size_count,
                    'resahape_img':reshape_img_base64,
                    'ref_object_detect':ref_object_detect
                }
    return {"message":"Get all data success.","data": result_dict}

    



    


    
    

