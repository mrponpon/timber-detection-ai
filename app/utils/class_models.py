# from app.utils import label_map_util_lite
from app.utils import detector_utils
from app.utils.export import run
from app.utils.augmentations import letterbox
from app.utils.general  import check_img_size ,non_max_suppression,scale_boxes,image2base64
from app.models.common import DetectMultiBackend
from app.utils.augmentations import classify_transforms
import torch
import torch.nn as nn
import torch.nn.functional as F
import os
import numpy as np
import cv2
import glob


class Yolov5_pytorch():
    def __init__(self,weights,device):
        # Load model
        self.device = detector_utils.select_device(device)
        # run(weights=weights,include=['torchscript'])
        self.model = DetectMultiBackend(weights,device=self.device)
        name = self.model.names
        self.names = list(name.values())

    def detect(self,im,img_size=(640, 640),conf_thres=0.25,iou_thres=0.45,max_det=2):
        imgsz = check_img_size(img_size, s=self.model.stride) 
        im0 = im.copy()
        im = letterbox(im,imgsz, stride=self.model.stride, auto=self.model.pt)[0]  # padded resize
        im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        im = np.ascontiguousarray(im)  # contiguous
        im = torch.from_numpy(im).to(self.model.device)
        im = im.half() if self.model.fp16 else im.float()  # uint8 to fp16/32
        im /= 255  # 0 - 255 to 0.0 - 1.0
        if len(im.shape) == 3:
            im = im[None]  # expand for batch dim
        pred = self.model(im, augment=True)
        pred = non_max_suppression(pred,conf_thres,iou_thres, None, False, max_det= max_det)
        predict_result = {}
        for n in self.names:
            predict_result[n] = []
        for i, det in enumerate(pred):  # detections per image
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_boxes(im.shape[2:], det[:, :4], im0.shape).round()
                # Write results
                for *xyxy, conf, cls in det:
                    x1, y1, x2, y2 = map(int, xyxy)
                    score = float('%.2f' % (conf)) *100
                    name = self.names[int(cls)]
                    temp_result = {
                            "ymin":y1,
                            "ymax":y2,
                            "xmin":x1,
                            "xmax":x2,
                            "score":score,
                            }
                    predict_result[name].append(temp_result)
        return predict_result

    def classify(self,im,img_size=(224, 224)):
        imgsz = check_img_size(img_size, s=self.model.stride)
        self.transforms = classify_transforms(imgsz[0])
        im = self.transforms(im)        
        im = torch.Tensor(im).to(self.model.device)
        im = im.half() if self.model.fp16 else im.float()  # uint8 to fp16/32
        if len(im.shape) == 3:
            im = im[None]  # expand for batch dim
        pred = self.model(im)
        pred = F.softmax(pred, dim=1)
        predict_result = {}
        for i, prob in enumerate(pred):
            topi = prob.argsort(0, descending=True)[0].tolist()
            name = f'{self.names[topi]}'
            score = f'{prob[topi]:.2f}'
            predict_result[name] = float(score)
        return predict_result
 